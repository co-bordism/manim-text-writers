from manim import *
from manim_text_writers import FancyTypeWriter, TypeWriter, WordByWord, KaraokeWriter
from manim_text_writers.utils import *


class TypeWriterSimple(Scene):
    def construct(self):
        the_text = Tex(
            "Typing text with a simple typewriter?",
            tex_template=TexFontTemplates.latin_modern_tw,
        )
        self.play(TypeWriter(the_text, time_per_letter=0.2))
        self.wait(1)


class TypeWriterFancy(Scene):
    def construct(self):
        the_text = Tex(
            "Typing text with a fancy typewriter?",
            tex_template=TexFontTemplates.latin_modern_tw,
        )
        self.play(FancyTypeWriter(the_text, time_per_letter=0.2))
        self.wait(1)


class KaraokeMode(Scene):
    def construct(self):
        the_text = Text("Rendering text in Karaoke mode?")
        self.play(KaraokeWriter(the_text, time_per_letter=0.2))
        self.wait(1)


class WordWriterFadeIn(Scene):
    def construct(self):
        the_text = Text(
            "Adding text word by word?\nWhy, yes. Of course you can do that.\nIt's easy."
        ).scale(0.5)
        for anim in WordByWord(the_text):
            self.play(anim)
        self.wait(1)


class WordWriterStamp(Scene):
    def construct(self):
        the_text = Text(
            "Adding text word by word?\nWhy, yes. Of course you can do that.\nIt's easy."
        ).scale(0.5)
        for anim in WordByWord(
            the_text,
            creation_anim=FadeInScaleDown,
            creation_anim_kwargs={"scale_from": 1.2},
        ):
            self.play(anim)
        self.wait(1)
