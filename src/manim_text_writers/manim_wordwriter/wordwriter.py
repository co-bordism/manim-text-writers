from manim import *
from manim_text_writers.utils import *


def WordByWord(
    text,
    creation_anim=None,
    delay_per_letter=0.06,
    comma_delay=0.25,
    sentence_delay=0.5,
    **kwargs
):
    if isinstance(text, SingleStringMathTex):
        raise TypeError("WordByWord is not implemented for Tex.")
    if not isinstance(text, Text):
        raise TypeError("WordByWord only works on Text mobjects.")

    list_of_words_as_text = text.original_text.split()
    list_of_words_as_mobjects = []
    i = 0
    for word in list_of_words_as_text:
        start = i
        end = i + len(word)
        list_of_words_as_mobjects.append(text[start:end])
        i = end
    if creation_anim is None:
        creation_anim = Appear

    anims = [
        creation_anim(x, run_time=0.25, **kwargs) for x in list_of_words_as_mobjects
    ]

    wait_times = []
    for word in list_of_words_as_text:
        n = len(word)
        # Calculate the delays between the words.
        # The longer the word, the longer the delay should be, with extra delays at end of a sentence.
        if word[-1] in [".", "!", "?"]:
            extra_time = sentence_delay
        elif word[-1] == ",":
            extra_time = comma_delay
        else:
            extra_time = 0
        delay = (delay_per_letter * n) + extra_time
        wait_times.append(delay)
    wait_anims = [Wait(x) for x in wait_times]
    all_animations = [anim for pair in zip(anims, wait_anims) for anim in pair]
    return all_animations
