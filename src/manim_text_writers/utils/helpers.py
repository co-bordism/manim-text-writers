from typing import Dict
from manim import *

# At the time of writing, AnimationGroup does not handle Wait() animations.
# Further, there is no Add() animation. This helper is a dummy Transform that hopefully does the same as
# scene.add(mob)
# scene.wait(time)
class AddAndWait(Transform):
    def create_target(self):
        target = self.mobject.copy()
        return target


class Appear(Transform):
    def create_target(self):
        target = self.mobject.copy()
        return target

    def begin(self):
        super().begin()
        self.starting_mobject.fade(1)


class FadeFromHalf(Transform):
    def create_target(self):
        target = self.mobject.copy()
        return target

    def begin(self):
        super().begin()
        self.starting_mobject.fade(0.5)


class FadeInScaleDown(Transform):
    """
    A creation animation that fades in and scales down.
    """

    def __init__(self, word, run_time=0.25, scale_from=2, **kwargs):
        self.scale_from = scale_from
        Transform.__init__(
            self,
            word,
            run_time=run_time,
            rate_func=rate_functions.ease_in_out_circ,
            **kwargs,
        )

    def create_target(self):
        target = self.mobject.copy()
        return target

    def begin(self):
        super().begin()
        self.starting_mobject.scale_in_place(self.scale_from)
        self.starting_mobject.fade(1)
